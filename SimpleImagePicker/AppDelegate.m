//
//  AppDelegate.m
//  SimpleImagePicker
//
//  Created by Berktas, Dogan on 6/7/13.
//  Copyright (c) 2013 Berktas, Dogan. All rights reserved.
//

#import "AppDelegate.h"

#import "TestViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.viewController = [[TestViewController alloc] init];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
