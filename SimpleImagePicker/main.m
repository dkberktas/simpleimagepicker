//
//  main.m
//  SimpleImagePicker
//
//  Created by Berktas, Dogan on 6/7/13.
//  Copyright (c) 2013 Berktas, Dogan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
