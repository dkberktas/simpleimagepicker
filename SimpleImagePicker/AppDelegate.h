//
//  AppDelegate.h
//  SimpleImagePicker
//
//  Created by Berktas, Dogan on 6/7/13.
//  Copyright (c) 2013 Berktas, Dogan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIViewController *viewController;

@end
